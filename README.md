## How to link with Bitbucket repo

You can link your project with Bitbucket repo

1. **Bitbucket:** create a repository **without README.md**, private
2. **Bitbucket:** Copy repository URL
3. **COMPUTER:** cd <project>
4. **COMPUTER:** git init
5. **COMPUTER:** git remote add origin <URL>
6. **COMPUTER:** git add .
7. **COMPUTER:** git commit -m "your message"
8. **COMPUTER:** git push -u origin master
9. **COMPUTER:** (work on your project)
10. **COMPUTER:** git add .
11. **COMPUTER:** git commit -m "my changes"
12. **COMPUTER:** git push
13. REPEAT 9 ~ 12

---

## How to download Professor's lab code 

1. **Class Homepage:** Click on the repository link
2. **Bitbucket:** Copy URL
3. **COMPUTER:** git clone <URL>
4. **COMPUTER:** cd <project>

---

## How to submit your homework 

* Please name your project repo in Bitbucket to **"HW#_<student_id>" (For example, HW2_6302321)**

1. Follow the instruction above to create your local project files and sync with Bitbucket with the correct project name
2. Make sure your Bitbucket repository is **Private**
3. Make sure your Bitbucket repository is shared with **mp2019f@gmail.com** with **admin access right**
4. Make sure you **never share your repository with other students**
5. By the deadline, push your homework to Bitbucket

* Make sure you commit all your changes and push to Bitbucket **BEFORE THE DEADLINE**. Any commits/pushes after the deadline will be ignored.